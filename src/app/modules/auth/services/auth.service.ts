import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of, tap, throwError } from 'rxjs';
import { LoginResponse } from '../dto/login-response';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {
    this.validateSession();
  }

  private _isAuthenticated: boolean = false;

  login(data: { email: string; password: string }): Observable<any> {
    console.log(data.email);
    return this.http
      .post<LoginResponse>('http://localhost:4000/api/v1/auth/login', data)
      .pipe(
        tap((response) => {
          console.log(response);
          localStorage.setItem('access_token', response.token);
          return (this._isAuthenticated = true);
        }),
        catchError((error) => {
          console.error('Login failed', error);
          // Return an observable with the error
          return throwError(() => new Error(error));
        })
      );
  }

  isAuthenticated(): boolean {
    return this._isAuthenticated;
  }

  validateSession(): void {
    // Retrieve the access token from local storage
    const accessToken = localStorage.getItem('access_token');

    if (accessToken) {
      // You can send a request to the server to validate the token
      // For simplicity, let's assume it's valid if it exists
      this.setAuthenticated(true);
    }
  }

  logout(): void {
    // Remove the access token from local storage
    this.setAuthenticated(false);
    localStorage.removeItem('access_token');
    // Update the authentication status
    console.log(this.isAuthenticated());
    this.router.navigate(['/auth/login']).then(() => {
      console.log('Navigation complete');
      console.log(this.isAuthenticated());
    });
  }

  setAuthenticated(value: boolean): void {
    this._isAuthenticated = value;
  }
}
