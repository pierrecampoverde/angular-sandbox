import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { onUnauthGuard } from './on-unauth.guard';

describe('onUnauthGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => onUnauthGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
