import { inject } from '@angular/core';
import {
  CanActivateFn,
  CanActivateChildFn,
  CanDeactivateFn,
  CanLoadFn,
  Router,
} from '@angular/router';
import { AuthService } from '../services/auth.service';

export const onAuthGuard: CanActivateFn | CanActivateChildFn = (
  route,
  state
) => {
  const authService = inject(AuthService);

  if (authService.isAuthenticated()) {
    return true;
  } else {
    // Create a URL tree to navigate to the login page if the user is not authenticated
    const router = inject(Router);
    return router.createUrlTree(['/auth/login']);
  }
};
