import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

export const onUnauthGuard: CanActivateFn = (route, state) => {
  const authService = inject(AuthService);
  const router = inject(Router);

  console.log(authService.isAuthenticated());

  if (authService.isAuthenticated()) {
    return router.createUrlTree(['/home']);
  } else {
    return true;
  }
};
