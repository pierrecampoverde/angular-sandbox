export interface LoginResponse {
  user: {
    id: string;
    email: string;
    name: string;
    last_name: string;
    profile_image: {
      id: number;
      name: string;
      created_at: string;
      user_id: string;
    };
  };
  token: string;
}
