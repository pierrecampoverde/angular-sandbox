import { Component } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { AuthService } from '../auth/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [ButtonModule],
  providers: [AuthService],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent {
  constructor(private authService: AuthService, private router: Router) {}
  logout() {
    this.authService.logout();
  }
}
